package sentryhandler

import (
	"strings"
	"time"

	raven "github.com/getsentry/raven-go"
	"github.com/grpc-ecosystem/go-grpc-middleware/tags"

	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// UnaryLogHandler handles access times and errors for unary RPC's
func UnaryLogHandler(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	start := time.Now()
	resp, err := handler(ctx, req)

	if err != nil {
		logGrpcErrorToSentry(ctx, info.FullMethod, start, err)
	}

	return resp, err
}

// StreamLogHandler handles access times and errors for stream RPC's
func StreamLogHandler(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	start := time.Now()
	err := handler(srv, stream)

	if err != nil {
		logGrpcErrorToSentry(stream.Context(), info.FullMethod, start, err)
	}

	return err
}

func stringMap(incoming map[string]interface{}) map[string]string {
	result := make(map[string]string)
	for i, v := range incoming {
		result[i] = fmt.Sprintf("%v", v)
	}
	return result
}

func methodToCulprit(methodName string) string {
	methodName = strings.TrimPrefix(methodName, "/gitaly.")
	methodName = strings.Replace(methodName, "/", "::", 1)
	return methodName
}

func generateRavenPacket(ctx context.Context, method string, start time.Time, err error) (*raven.Packet, map[string]string) {
	grpcErrorCode := grpc.Code(err)

	if grpcErrorCode == codes.OK {
		return nil, nil
	}

	tags := grpc_ctxtags.Extract(ctx)
	ravenDetails := stringMap(tags.Values())

	ravenDetails["grpc.code"] = grpcErrorCode.String()
	ravenDetails["grpc.method"] = method
	ravenDetails["grpc.time_ms"] = fmt.Sprintf("%.0f", time.Since(start).Seconds()*1000)
	ravenDetails["system"] = "grpc"

	// Skip the stacktrace as it's not helpful in this context
	packet := raven.NewPacket(err.Error(), raven.NewException(err, nil))
	packet.Culprit = methodToCulprit(method)
	return packet, ravenDetails
}

func logGrpcErrorToSentry(ctx context.Context, method string, start time.Time, err error) {
	packet, tags := generateRavenPacket(ctx, method, start, err)
	if packet == nil {
		return
	}

	raven.Capture(packet, tags)
}
